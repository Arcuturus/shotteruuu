var souls = [];
console.log(souls);

/**
  The button click function
*/
$('#add-name').click(function(){
  var soulName = document.getElementById("name-text").value;
  if (soulName.length >= 3) {
    if (soulName.includes(",")) {
      var names = soulName.split(",");
      for (name of names) {
        var n = $.trim(name)
        if(name.length > 0) {
          souls.push(n);
          addSoulName(n);
        }
        else {
          // TODO: display warning, omitted empty name
        }
      }
    } else {
      souls.push(soulName);
      addSoulName(soulName);
    }
    document.getElementById("name-text").value = '';
    saveSouls();

  }
});

$('#continue').click(function(){
    // save random time settings
    var low = parseInt(document.getElementById("start-time").value);
    var end = parseInt(document.getElementById("end-time").value);
    Cookies.set("low", low);
    Cookies.set("end", end);

    var wheellow = parseInt(document.getElementById("start-wheels").value);
    var wheelend = parseInt(document.getElementById("end-wheels").value);
    Cookies.set("wheellow", wheellow);
    Cookies.set("wheelend", wheelend);

  var location = '/drink.html';
  window.location.href = location;
  //var soulsString = soulNamesToJSON();
  //document.make
  console.log(souls);
});

function bindEnterClick() {
  // Get the input field
  var input = document.getElementById("name-text");

  // Execute a function when the user releases a key on the keyboard
  input.addEventListener("keyup", function(event) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Trigger the button element with a click
      document.getElementById("add-name").click();
    }
  });
}

/**
  Saving souls from
*/
function saveSouls() {
    console.log(souls);
  console.log("Trapping souls...");
  var obj = { "souls": souls };
  var encoded = JSON.stringify(obj);
  Cookies.set("souls", encoded);
  console.log("Trapped ("+souls.length+") souls in our horcrux.");
}

/**
  Loading souls into the `souls` global variable
*/
function loadSouls() {
  console.log("Loading souls...");
  var obj = Cookies.get("souls");
  var decoded = JSON.parse(obj);
  souls = decoded["souls"];
  console.log("Preparing ("+souls.length+") souls for limbo.");
}

/**
  Get random number
    low!: inclusive
    end: inclusive
*/
function randomRange(low, high) {
  return Math.floor((Math.random() * (high + 1 - low)) + low);
}

/**
  Display an alert box after 3 seconds (3000 milliseconds):
    setTimeout(function(){ alert("Hello"); }, 3000);
*/
function getRandomInterval2() {
  // operate in ms
    //var low = 10*60*1000 // 10 mins
    //var end   = 15*60*1000 // // 15 mins
  var low = parseInt(Cookies.get("low"))*60*1000;
    var end = parseInt(Cookies.get("end"))*60*1000;
  var randomInterval = randomRange(low, end);
  return randomInterval;
}

/**
  Add a soul to the name container in the HTML
*/
function addSoulName(name) {
  if(name.length < 1) {
    // TODO: display warning
  }
  else {
      $(".name-container ul").append('<li id="name">' + name + '</li>');
  }
}


$('#back').click(function(){
  var location = '/home.html';
  window.location.href = location;
  //var soulsString = soulNamesToJSON();
  //document.make
});

/**
  Remove all soul names
*/
function removeAllSoulNames() {
  souls = [];
  saveSouls();
  location.reload();
}

var getRandInterval = function() {
  return getRandomInterval2(); // between 10 and 15 minutes
};
var warningTime = 10*1000; // in ms
var spinWheelDelay = 400; // ms
var debug = true;
var wheels = [];
//var redoverLay = false;
var audio = new Audio("/sound/shotteruuuuuu.mp3");

function shotteruuuWarning(callback) {
  if (debug) {
    callback();
    return;
  }
  var body = document.getElementsByTagName("body")[0];
  body.style = "background-color: red;";
  setTimeout(function() {
    body.style = "background-color: white;";
    callback();
  }, warningTime);
}

loadSouls();
console.log("Loaded ("+souls.length+") souls");
for (var i = 0; i < souls.length; i++) {
    addSoulName(souls[i]);
    console.log("adding");
}

if (document.getElementById("start-time") !== null) {
    document.getElementById("start-time").value = Cookies.get("low");
    document.getElementById("end-time").value = Cookies.get("end");
    document.getElementById("start-wheels").value = Cookies.get("wheellow");
    document.getElementById("end-wheels").value = Cookies.get("wheelend");
}

/**
  So it begins
*/
function beginJudgement() {
  loadSouls();
  //var randInterval = getRandInterval(); // 1 second // getRandomInterval(); // between 10 and 15 minutes

  // Start a function that fires after `randInterval`-seconds.
  // This calls the function `spinThatShit` with the global variable souls.
  setTimeout(function() {
    var minutesAsStr = Math.round(100/1000, 1).toString();
    console.log("Beginning judgement after (" + minutesAsStr + ") seconds...");
    spinThatShit(souls);
  }, 100);
}

/**
  SPIN THAT SHIT
    https://www.youtube.com/watch?v=PGNiXGX2nLU
*/

function spinThatShit(souls) {
    var wheelLow = Cookies.get("wheellow");
    var wheelhigh = Cookies.get("wheelend");
    console.log("wheellow: " + wheelLow.toString() + ", wheelhigh: " + wheelhigh.toString());
    var r_range = randomRange(parseInt(wheelLow), parseInt(wheelhigh));
  //console.log(r_range);

  if (wheels.length > 0) {
    for (var i = 0; i < wheels.length; i++) {
      var id = wheels[i].canvasName;
      //console.log("id: " + id);
      var oldcanv = document.getElementById(id);
      document.getElementById("lollygag").removeChild(oldcanv);
    }
  }
  wheels = [];
  //console.log("wheel length: " + wheels.length.toString());
    //
    //var randInterval = getRandInterval(); // 1 second // getRandomInterval(); // between 10 and 15 minutes
    var nextInterval= getRandInterval();
    console.log("Next shot in " + (nextInterval/60/1000).toString());

  for (var i = 0; i < r_range; i++) {
    var wheel = new RouletteWheel(souls, "canvas"+(i+1).toString(), randomRange(0, 360), function() {
      setTimeout(function() {
        location.reload();
      }, nextInterval);
    });
    wheel.drawRouletteWheel();
    wheels.push(wheel);
  }

  var index = 0;
  var playing = false;
  function spinWheel() {
    if (!playing) {
        //if (!debug) {
          audio.play();
        //}
        playing = true;
    }
    if (index < wheels.length) {
      wheels[index].spin();
      index++;
      setTimeout(function() {
        spinWheel();
      }, spinWheelDelay);
    }
  }
  shotteruuuWarning(spinWheel);
}

//##################### ROULETTE FUNCTIONS ##############################

class RouletteWheel {
  constructor(souls, canvName, startAngle, onFinishedCallback) { //, startAngle, arc, spinTimeout, spinArcStart, spinTime, spinTimeTotal) {
    this.options = souls;
    //console.log(this.options);
    //console.log(canvName);
    this.onWheelFinished = onFinishedCallback;

    this.startAngle = startAngle;
    this.arc = Math.PI / (this.options.length / 2);
    this.spinTimeout = null;

    this.spinArcStart = 10;
    this.spinTime = 0;
    this.spinTimeTotal = 0;

    this.wheelSpinTime = 15;
    this.audio = null;

    this.canvasName = canvName;
    var canv = document.createElement('canvas');
    canv.id = this.canvasName;
    canv.height = 600;
    canv.width = 600;
    document.getElementById("lollygag").appendChild(canv);

    this.ctx;
  }

  byte2Hex(n) {
    var nybHexString = "0123456789ABCDEF";
    return String(nybHexString.substr((n >> 4) & 0x0F,1)) + nybHexString.substr(n & 0x0F,1);
  }

  RGB2Color(r,g,b) {
  	return '#' + this.byte2Hex(r) + this.byte2Hex(g) + this.byte2Hex(b);
  }

  getColor(item, maxitem) {
    var phase = 0;
    var center = 128;
    var width = 127;
    var frequency = Math.PI*2/maxitem; // change to 3?

    var red   = Math.sin(frequency*item+2+phase) * width + center;
    var green = Math.sin(frequency*item+0+phase) * width + center;
    var blue  = Math.sin(frequency*item+4+phase) * width + center;

    return this.RGB2Color(red, green, blue);
  }

  drawRouletteWheel() {
    //console.log("Drawing roulette wheel...");
    var canvas = document.getElementById(this.canvasName); //mb canvName
    if (canvas.getContext) {
      var outsideRadius = 294;
      var textRadius = 200;
      var insideRadius = 115;
      var radius = 300;

      this.ctx = canvas.getContext("2d");
      this.ctx.clearRect(0,0,radius*2,radius*2);

      this.ctx.strokeStyle = "black";
      this.ctx.lineWidth = 2;

      this.ctx.font = 'bold 28px Helvetica, Arial';

      for(var i = 0; i < this.options.length; i++) {
        var angle = this.startAngle + i * this.arc;
        //ctx.fillStyle = colors[i];
        this.ctx.fillStyle = this.getColor(i, this.options.length);

        this.ctx.beginPath();
        this.ctx.arc(radius, radius, outsideRadius, angle, angle + this.arc, false);
        this.ctx.arc(radius, radius, insideRadius, angle + this.arc, angle, true);
        this.ctx.stroke();
        this.ctx.fill();

        this.ctx.save();
        this.ctx.shadowOffsetX = -1;
        this.ctx.shadowOffsetY = -1;
        this.ctx.shadowBlur    = 0;
        this.ctx.shadowColor   = "rgb(220,220,220)";
        this.ctx.fillStyle = "black";
        this.ctx.translate(radius + Math.cos(angle + this.arc / 2) * textRadius,
                      radius + Math.sin(angle + this.arc / 2) * textRadius);
        this.ctx.rotate(angle + this.arc / 2 + Math.PI * 2);
        var text = this.options[i];
        this.ctx.fillText(text, -this.ctx.measureText(text).width / 2, 0);
        this.ctx.restore();
      }

      //Arrow
      this.ctx.fillStyle = "black";
      this.ctx.beginPath();
      this.ctx.moveTo(radius - 4, radius - (outsideRadius + 5));
      this.ctx.lineTo(radius + 4, radius - (outsideRadius + 5));
      this.ctx.lineTo(radius + 4, radius - (outsideRadius - 5));
      this.ctx.lineTo(radius + 9, radius - (outsideRadius - 5));
      this.ctx.lineTo(radius + 0, radius - (outsideRadius - 13));
      this.ctx.lineTo(radius - 9, radius - (outsideRadius - 5));
      this.ctx.lineTo(radius - 4, radius - (outsideRadius - 5));
      this.ctx.lineTo(radius - 4, radius - (outsideRadius + 5));
      this.ctx.fill();
    } else {
      //console.log("Could not init canvas context for canvas: " + this.canvasName);
    }
  }

  spin() {
      //console.log("Do the spin spin spin");
    for (var i = 0; i < 20; i++) {
        this.spinAngleStart = randomRange(0, 360); //Math.random() * 10 + 10;
    }
    this.spinTime = 0;
    this.spinTimeTotal = 7*1000//Math.random() * 3 + 4 * 1000;
    this.rotateWheel();

  }

  rotateWheel() {
    //console.log("Rotating shit");
    this.spinTime += this.wheelSpinTime;
    if(this.spinTime >= this.spinTimeTotal) {
      this.stopRotateWheel();
      return;
    }
    var spinAngle = this.spinAngleStart - this.easeOut(this.spinTime, 0, this.spinAngleStart, this.spinTimeTotal);
    this.startAngle += (spinAngle * Math.PI / 180);
    this.drawRouletteWheel();

    var self = this;
    this.spinTimeout = setTimeout(function() {
      self.rotateWheel();
    }, this.wheelSpinTime);
  }

  stopRotateWheel() {
    var radius = 300;
    clearTimeout(this.spinTimeout);
    var degrees = this.startAngle * 180 / Math.PI + 90;
    var arcd = this.arc * 180 / Math.PI;
    var index = Math.floor((360 - degrees % 360) / arcd);
    this.ctx.save();
    this.ctx.font = 'bold 30px Helvetica, Arial';
    var text = this.options[index]
    this.ctx.fillText(text, radius - this.ctx.measureText(text).width / 2, radius + 10);
    this.ctx.restore();

    this.onWheelFinished();
  }

  easeOut(t, b, c, d) {
    var ts = (t/=d)*t;
    var tc = ts*t;
    return b+c*(tc + -3*ts + 3*t);
  }
}
